package cl.erickbalda.indicador.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import cl.erickbalda.indicador.model.Indicadores;
import cl.erickbalda.indicador.repository.IndicadoresRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("v1/api")
public class IndicadorController {

    @Autowired
    IndicadoresRepository indicadoresRepository;

    /**
     * Listado de resultado encuestas
     * 
     * @throws ParseException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    @ApiOperation(value = "getIndicadorByCodigoAndFecha", notes = "busqueda de indicador por codigo y fecha")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = Indicadores.class),
            @ApiResponse(code = 400, message = "Bad Request. Parametro invalido", response = Indicadores.class),
            @ApiResponse(code = 405, message = "Metodo no permitido", response = Indicadores.class) })
    @GetMapping("/indicador/{tipo_indicador}/{dd-mm-yyyy}")
    public ResponseEntity<Indicadores> getIndicadorByCodigoAndFecha(@PathVariable("tipo_indicador") String codigo,
            @PathVariable("dd-mm-yyyy") String fecha)
            throws ParseException {

        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
        Optional<Indicadores> indicadoresData = indicadoresRepository.findIndicador(codigo, date);

        if (indicadoresData.isPresent()) {
            return new ResponseEntity<>(indicadoresData.get(), HttpStatus.OK);
        } else {

            RestTemplate restTemplate = new RestTemplate();
            String response = restTemplate.getForObject("https://mindicador.cl/api/" + codigo + "/" + fecha,
                    String.class);

            // IMPLEMENTACION INCOMPLETA PARA GUARDADO Y RETORNO DE DATA PARA CONSULTA
            // EXTERNA
            // indicadoresRepository.save(valor);

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Guarda nueva tarea
     */
    @ApiOperation(value = "loadIndicadorValor", notes = "Guarda nueva valor")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Indicador guardado o actualziado", response = Indicadores.class),
            @ApiResponse(code = 400, message = "Bad Request. Parametro invalido", response = Indicadores.class),
            @ApiResponse(code = 405, message = "Metodo no permitido", response = Indicadores.class) })
    @PostMapping("/indicador")
    public ResponseEntity<Indicadores> loadIndicadorValor(@Valid @RequestBody Indicadores indicadores) {
        try {
            Indicadores valor = new Indicadores(indicadores.getCodigo(), indicadores.getNombre(),
                    indicadores.getUnidad_medida(), indicadores.getFecha(), indicadores.getValor());
            indicadoresRepository.save(valor);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
