package cl.erickbalda.indicador.repository;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cl.erickbalda.indicador.model.Indicadores;

public interface IndicadoresRepository extends JpaRepository<Indicadores, String>{

    @Query("SELECT i FROM Indicadores i WHERE i.codigo = ?1 AND i.fecha = ?2")
    Optional<Indicadores> findIndicador(String codigo, Date fecha);
    
}

