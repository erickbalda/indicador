DROP TABLE IF EXISTS indicadores;


CREATE TABLE indicadores (
  codigo VARCHAR(250) PRIMARY KEY,
  nombre VARCHAR(250) NOT NULL,
  unidad_medida VARCHAR(250) NOT NULL,
  fecha DATETIME NOT NULL,
  valor DECIMAL(20, 2) NOT NULL
);


INSERT INTO indicadores (codigo, nombre, unidad_medida,fecha,valor) VALUES
  ('uf','Unidad de fomento (UF)','Pesos','2021-08-16T04:00:00.000Z',29819.89),
  ('ivp','Indice de valor promedio (IVP)','Pesos','2021-08-16T04:00:00.000Z',29819.89),
  ('dolar','Dólar observado','Pesos','2021-08-16T04:00:00.000Z',29819.89),
  ('dolar_intercambio','Dólar acuerdo','Pesos','2021-08-16T04:00:00.000Z',29819.89),
  ('euro','Euro','Pesos','2021-08-16T04:00:00.000Z',29819.89),
  ('ipc','Indice de Precios al Consumidor (IPC)','Porcentaje','2021-08-16T04:00:00.000Z',29819.89);


