# CHALLENGE

## Version Backend
-   Spring Boot 2.5.3

## Requerimientos Minimos 
-   Java 11
-   Maven 3.0+


## Deploy Backend

1.   Descargar proyecto 
 
2.   Abrir terminal en la ruta principal del proyecto

3.   Ingresar en la ruta \backend y ejecutar el siguiente comando

            mvn spring-boot:run



## Testing Backend

1. una vez ejecutado el sistema se genera documentacion y testing con Swagger, se puede consultar a traves de la URL http://localhost:8080/swagger-ui.html

2. Se anexa en la ruta principal del proyecto archivo para importacion de prueba de endpoints y Test en programa POSTMAN 

INDICADOR.postman_collection.json

##Desafío técnico de microservicios 

¿Por qué? Buscamos miembros del equipo de alto rendimiento. Entonces, para conocerte un poco, hemos creado un desafío.

El reto:

Estamos buscando una forma de proveer lo indicadores económicos en linea y respaldados localmente para no saturar servicio externo por eficiencia en costos del consumo de una api externa.

La Api de servicio rest debe tener una operación que permita consultar una fecha y el indicador deseado, buscar en una bd local (o archivo de texto) el indicador , sólo sino lo tiene debe obtenerla con un servicio externo (https://mindicador.cl/) y actualizar en bd local para que la próxima consulta sea resuelta desde la bd local. La Api debe tener una operación que permitir alimentarla de los indicadores locales para una fecha especifica, los cuales deben persistir en la misma tabla local en un motor como mysql, Oracle, postgrest, o archivo plano, segun el paso anterior. La Api debe considerar mostrar su descripción funcional en swagger.

Las normas: Aplique normas de servicios restfull Bifurca este repositorio y crea un Pull request Aplique Patrones de diseño que estime conveniente Utilice lenguaje para microservicios Java framework springboot. Debe utilizar Maven o equivalente para las dependencias en Java Proyecto IDE Eclipse o STS. Agregue una prueba unitaria a su aplicación (tanto como pueda)

Tiempo: Tienes 3 horas como máximo si necesitas más tiempo, ¡no te preocupes empuja tus cambios! es más importante la calidad que terminarla
